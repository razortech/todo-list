import React from 'react';
import { FaTrashAlt } from "react-icons/fa";

function TodoItem({ task, onDelete }) {
    return (
        <li className='todo-list__item flex flex-row justify-between items-center p-4 rounded-md my-4 bg-gradient-to-r from-sky-500 to-violet-500'>
            <span className="inline-flex items-center">
                <input type='checkbox' className="w-5 h-5 mr-4" />
                <p className='todo-list__item--text text-white font-semibold'>{task}</p>
            </span>
            <FaTrashAlt className="text-white cursor-pointer" onClick={onDelete} />
        </li>
    )
}

export default TodoItem;