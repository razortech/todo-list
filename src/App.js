import React, { useState } from 'react';
import TodoItem from './components/TodoItem';

/* 
Features to add:
- Light and Dark mode
- Edit todo item
- Description for todo item
*/

function App() {
    const [task, setTask] = useState('');
    const [tasks, setTasks] = useState([]);

    // Set the state of the text being changed in the input
    const handleChange = (e) => {
        setTask(e.target.value);
    }

    const handleClick = () => {
        if (!task.trim()) return;
        // Add the task to the array
        setTasks([...tasks, task]);
        // Clear the input
        setTask('');
    }

    const handleDelete = (index) => {
        setTasks(tasks.filter((_, idx) => idx !== index));
    }

    return (
        <div className="flex flex-col items-center justify-center h-screen w-screen bg-[#242A43]">
            <h1 className="text-6xl text-white font-bold uppercase mb-10">TODO List</h1>
            <div className='todo-list-container flex flex-col items-center justify-center mb-10 w-[700px]'>
                <ul className='outter-container rounded-md px-4 bg-gray-600 w-full'>
                    {tasks.map((tsk, idx) => (
                        <TodoItem key={idx} task={tsk} onDelete={() => handleDelete(idx)} />
                    ))}
                </ul>
            </div>
            {/* Button to add Task */}
            <div className="flex flex-row">
                <input
                    type="text"
                    className="border p-2 rounded-l-md focus:outline-none w-[400px]"
                    placeholder="Your text here..."
                    value={task}
                    onChange={handleChange}
                />
                <button type='button' className="bg-gradient-to-r from-sky-500 to-violet-500 text-white py-2 px-4 rounded-r-md hover:bg-blue-700 focus:outline-none" onClick={handleClick}>
                    Add Task
                </button>
            </div>

        </div>
    );
}

export default App;
